<?php

/**
 * Nachdem wir uns in der Mittagspause mit einem kleinen aktivierenden Spiel beschäftigt
 * haben, vertiefen wir unsere Kenntnisse in TDD.
 */

$tests = array (
	array(1, 1),
	array(2, 2),
	array(3, 'fizz'),
	array(4, 4),
	array(5, 'buzz'),
	array(6, 'fizz'),
	array(10, 'buzz'),
	array(15, 'fizzbuzz'),
	array(16, 16),
	array(30, 'fizzbuzz'),
	);

foreach ($tests as $test) {
	$input = $test[0];
	$expected = $test[1];
	$output = fizzbuzz($input);
	echo $input."\t".$output."\t".$expected."\t";
	echo ($output === $expected) ? "OK\n" : "NEEEE\n";	
}


function fizzbuzz($input) {
	if ($input % 3 == 0 && $input % 5 == 0)
		return 'fizzbuzz';
	if ($input % 5 == 0)
		return 'buzz';
	if ($input % 3 == 0)
		return 'fizz';
	return $input;
}