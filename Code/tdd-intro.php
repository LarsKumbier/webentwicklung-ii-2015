<?php

/**
 * Erster Einstieg in Test-Driven-Development mit ein paar Learning Tests über
 * die Art und Weise, wie PHP Variablen vergleicht.
 */


$tests = array (
	array(null, null, true),
	array(  '', null, true),
	array(   0, null, true),
	array(   0,   '', true),
	array(   0,  '0', true),
	array(   0,    1, false),
	);

$testsExplicit = array (
	array(null, null, true),
	array(  '', null, false),
	array(   0, null, false),
	array(   0,   '', false),
	array(   0,  '0', false),
	array(  '',   '', true),
	array(   0,    1, false),
	);


run($tests, 'compare');
run($testsExplicit, 'compareExplicit');

function run($data, $functionUnderTest) {
	foreach ($data as $test) {
		$a = $test[0];
		$b = $test[1];
		$expected = $test[2];

		$output = $functionUnderTest($a, $b);
		$result = $output === $expected;
		printResult($functionUnderTest, $a, $b, $expected, $result);
	}
}

function printResult($name, $input1, $input2, $expected, $result)
{
	echo str_pad($name.'('.$input1.','.$input2.') => '.$expected, 32);
	echo '['.($result ? 'ok' : '!!').']';
	echo "\n";
}

function compare($a, $b) {
	return $a == $b;
}

function compareExplicit($a, $b) {
	return $a === $b;
}