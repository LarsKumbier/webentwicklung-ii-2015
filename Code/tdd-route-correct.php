<?php

class RouteTest extends \PHPUnit_Framework_TestCase
{
	/**
	 * @group unit
	 */
	public function test_RouterFix()
	{
		$routefix = new Router_Fix('/moo', 'XController');
		$actual = $routefix->resolve('/moo');
		$this->assertSame('XController', $actual);

		$actual = $routefix->resolve('/nonexistant');
		$this->assertFalse($actual);
	}

	public function test_RouterFixIgnoresCase()
	{
		$routefix = new Router_Fix('/Moo', 'XController');
		$actual = $routefix->resolve('/moo');
		$this->assertSame('XController', $actual);
	}


	/**
	 * @group unit
	 */
	public function test_RouterRegexp()
	{
		$routeRegexp = new Router_RegExp('/^\/user[0-9]$/', 'YController');
		$actual = $routeRegexp->resolve('/user1');
		$this->assertSame('YController', $actual);

		$actual = $routeRegexp->resolve('/userM');
		$this->assertFalse($actual);
	}


	public function test_Integration()
	{
		// =========== Arrange ===========
		$route = new Router();
		$route->addRoute(new Router_Fix('/40Jahre', 'StaticController'))
		      ->addRoute(new Router_Fix('/SpecialAktion', 'StaticController'))
		      ->addRoute(new Router_RegExp('/^\/user\/[0-9]{0,6}\/?$/', 'UserController'));
		
		// Kombination: (1) Act -> (2) Assert
		$url = '/40jahre';
		$result = $route->getResponsibleController($url);
		$this->assertEquals('StaticController', $result);

		$url = '/SpecialAktion';
		$result = $route->getResponsibleController($url);
		$this->assertEquals('StaticController', $result);

		$url = '/user/198763';
		$result = $route->getResponsibleController($url);
		$this->assertEquals('UserController', $result);

		$url = '/user/';
		$result = $route->getResponsibleController($url);
		$this->assertEquals('UserController', $result);

		$url = '/user/1234/'; 
		$result = $route->getResponsibleController($url);
		$this->assertEquals('UserController', $result);

		$url = '/user/1234/asdf'; 
		$result = $route->getResponsibleController($url);
		$this->assertFalse($result);

		// Default Verhalten testen: Wenn nichts gefunden, dann False.
		// Alternative: 404Controller? ErrorController404?
		$url = '/non-existant-path/go/to/hell';
		$result = $route->getResponsibleController($url);
		$this->assertFalse($result);
	}

	/**
	 * @dataProvider dp_Regexp
	 * @group learning
	 */
	public function test_LearningRegexp($regexp, $input, $expected)
	{
		$actual = preg_match($regexp, $input);
		$this->assertSame($expected, $actual);
	}

	/**
	 * @group learning
	 */
	public function dp_Regexp()
	{
		return array (
			array("/foo/", "foo", 1),				// foo beinhaltet foo
			array("/foo/", "barfoobar", 1),			// barfoobar beinhaltet foo
			array("/^foo/", "barfoobar", 0),		// barfoobar beginnt nicht mit foo
			array("/^foo/", "foobar", 1),			// foobar beginnt mit foo
			array("/^foo/", "Foobar", 0),			// regexp sind standard case-sensitive...
			array("/^foo/i", "Foobar", 1),			// ... können aber auch insensitive gemacht werden
			array("/foo$/", "Foobar", 0),			// Foobar endet mit foo
			array("/foo$/", "Foobarfoo", 1),		// 
			array("/^\/user\//", '/user/1', 1),		// 
			array("/^\/user\//", '/profil/1', 0),	// 
			array("/^\/user\/[0-6]/", '/user/1', 1),// 
			array("/^\/user\/[0-6]/", '/user/7', 0),// 
			array("/^\/user\/[0-6]/", '/user/9999', 0),// 
			array("/^\/user\/[0-9]/", '/user/9999', 1),// 
			array("/^\/user\/[0-9]$/", '/user/09', 0),// 
			array("/^\/user\/[0-9]{0,5}$/", '/user/09234', 1),// 
			array("/^\/user\/[0-9]{0,5}$/", '/user/3', 1),// 
			array("/^\/user\/[0-9]{0,5}$/", '/user/', 1),// 
			array("/^\/user\/[0-9]{0,5}$/", '/user/3/', 0),// 
			array("/^\/user\/[0-9]{0,5}\/$/", '/user/3/', 1),// 
			array("/^\/user\/[0-9]{0,5}\/$/", '/user/3', 0),// 
			array("/^\/user\/[0-9]{0,5}\/?$/", '/user/3', 1),// 
			array("/^\/user\/[0-9]{0,5}\/?$/", '/user/3/', 1),// 
			array("/^\/user\/[0-9]{0,5}\/?$/", '/user/3//', 0),// 
			);
	}
}

/**
 * Pattern: Chain Of Responsibility
 */
class Router {
	protected $routes = array();

	public function addRoute(IRoute $route) 
	{
		$this->routes[] = $route;

		return $this;
	}

	public function getResponsibleController($path) 
	{
		foreach ($this->routes as $registeredRoute) {
			$result = $registeredRoute->resolve($path);
			if ($result !== false)
				return $result;
		}

		return false;
	}
}

interface IRoute 
{
	public function __construct($path, $controller);
	public function resolve($path);
}

abstract class Router_Abstract implements IRoute
{
	protected $path;
	protected $controller;

	public function __construct($path, $controller) 
	{
		if (!is_string($path) || !is_string($controller))
			throw new \InvalidArgumentException('Parameters have to be string');
		$this->path = $path;
		$this->controller = $controller;
	}
}

class Router_Fix extends Router_Abstract
{
	public function resolve($path) 
	{
		if (strtolower($path) == strtolower($this->path))
			return $this->controller;
		return false;
	}
}

class Router_Regexp extends Router_Abstract
{
	public function resolve($path) 
	{
		if (preg_match($this->path, $path))
			return $this->controller;
		return false;
	}
}

