<?php

namespace Post;

/**
 * /post/      => Alle Posts anzeigen, vielleicht in Kurzform (Übersicht)
 * /post/15    => Den Post mit der ID 15 anzeigen
 */
class Controller
{
	public function __construct($path, $pdo)
	{
		throw new \Exception('Implement Me!');

		// @todo: DomainMapper initialisieren
	}

	public function run()
	{
		// @todo: entscheiden, was der User von uns möchte
		// @todo: Übersicht: DomainMapper alle Posts holen lassen
		// @todo: Spezieller Post: nur den Post fetchen
		// @todo: Übergabe der/des Models an die entsprechende View - ODER - Anzeigen*
		//        * keine gute Lösung
	}
}