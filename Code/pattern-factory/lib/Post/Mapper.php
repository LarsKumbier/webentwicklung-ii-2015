<?php

namespace Post;

class Mapper
{
	protected $pdo;
	protected $querySelect = 'SELECT title,content,posttime,id FROM posts';
	protected $queryDelete = 'DELETE FROM posts WHERE id=:id';
	protected $queryInsert = 'INSERT INTO posts (title,content,posttime,id) VALUES (:title, :content, :posttime, :id)';


	public function __construct(\PDO $pdo) 
	{
		$this->pdo = $pdo;
	}


	/**
	 * @param int
	 * @return \Post\Model
	 */
	public function fetch($id)
	{
		$query = $this->pdo->prepare($this->querySelect . ' WHERE id=:id');
		$result = $query->execute(array(':id' => $id));
		
		if (!$result)
			return null;

		$row = $query->fetch();

		return $this->createModel($row);
	}


	/**
	 * @return array<\Post\Model>
	 * @todo   opt. Parameter limit
	 * @todo   opt. Parameter offset
	 */
	public function fetchAll()
	{
		$query = $this->pdo->prepare($this->querySelect);
		$result = $query->execute();
		if (!$result)
			return null;

		$rows = $query->fetchAll();
		if (empty($rows))
			return null;

		$models = array();
		foreach ($rows as $key => $row)
			$models[$row['id']] = $this->createModel($row);

		return $models;
	}


	/**
	 * @param \Post\Model
	 * @todo  return sollte die zuletzt eingefügte ID zurückgeben
	 */
	public function insert(Model $model)
	{
		$query = $this->pdo->prepare($this->queryInsert);
		$vars = $model->toArray();
		$sqlVars = array();
		foreach ($vars as $key => $value)
			$sqlVars[':'.$key] = $value;
		unset($vars);

		$result = $query->execute($sqlVars);
		if ($result !== 1)
			throw new Exception('Could not insert Model into database: '.implode(' | ', $sqlVars));

		return $this;
	}


	/**
	 * @param  int
	 * @return this
	 * @todo   Fehlerbehandlung - was passiert, wenn die ID nicht existiert? (Exception)
	 */
	public function delete($id)
	{
		if (is_a('\Post\Model', $id))
			$id = $id->id;

		$query = $this->queryDelete;
		$result = $query->execute(array(':id' => $id));

		return $this;
	}


	/**
	 * @param array
	 */
	public function createModel($data)
	{
		if (!is_array($data))
			throw new InvalidArgumentException('Expected data array');

		$model = new Model(
			$data['title'],
			$data['content'],
			$data['posttime'],
			$data['id']);

		return $model;
	}
}
