<?php

namespace Post;


class Model
{
	use \Traits\MagicSetterGetter;

	protected $_id;
	protected $_posttime;
	protected $_title;
	protected $_content;

	public function __construct($title, $content, $posttime=null, $id=null)
	{
		$this->setTitle($title)
		     ->setContent($content)
		     ->setPosttime($posttime);
		$this->_id = $id;
	}

	public function setTitle($new)
	{
		return $this->SetIfNotEmptyString('title', $new);
	}

	public function setContent($new) 
	{
		return $this->SetIfNotEmptyString('content', $new);
	}

	/**
	 * @param DateTime|string|null - When was the post posted?
	 * @return $this;
	 */
	public function setPosttime($time)
	{
		if (is_a('\DateTime', $time) || $time === null) {
			$this->_posttime = $time;
			return $this;
		}

		if (is_string($time)) {
			$datetime = new \DateTime($time);
			$this->_posttime = $datetime;
			return $this;
		}

		throw new \InvalidArgumentException('Posttime needs to be a DateTime Object or a parseable String.');
	}

	protected function SetIfNotEmptyString($name, $new) 
	{
		$property = '_'.$name;
		$new = (string)$new;
		if (empty($new))
			throw new \InvalidArgumentException("$name is supposed to be a non-empty string");
		$this->$property = $new;

		return $this;
	}


	public function toArray()
	{
		return get_object_vars($this);
	}
}