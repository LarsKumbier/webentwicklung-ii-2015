<?php

namespace Traits;

/**
 * Usage: Define your properties via `protected $_id` or similar
 */
trait MagicSetterGetter
{
	/**
	 * erlaubt einfache Zugriffe über
	 * 
	 * @example $model->id
	 * @example $model->content
	 * @return  mixed
	 */
	public function __get($name)
	{
		$method = 'get'.ucfirst($name);      // id => getId
		if (method_exists($this, $method))
			return $this->$method();

		$property = '_'.$name;
		if (property_exists($this, $property))
			return $this->$property;

		trigger_error("Property $name does not exist.", E_USER_NOTICE);
		return null;
	}

	/**
	 * erlaubt einfache Zugriff über 
	 *
	 * @example $model->id = 15 
	 * @example $model->title = 'moo'
	 * @return  $this
	 */
	public function __set($name, $value)
	{
		$method = 'set'.ucfirst($name);      // id => setId
		if (method_exists($this, $method))
			return $this->$method($value);

		$property = '_'.$name;
		if (property_exists($this, $property)) {
			$this->$property = $value;
			return $this;
		}

		trigger_error("Property $name does not exist.", E_USER_NOTICE);
		$this->$name = $value;
		return $this;
	}
}