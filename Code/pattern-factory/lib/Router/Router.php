<?php

namespace Router;

/**
 * Pattern: Chain Of Responsibility
 */
class Router {
	protected $routes = array();

	public function addRoute(IRoute $route) 
	{
		$this->routes[] = $route;

		return $this;
	}

	public function getResponsibleController($path) 
	{
		foreach ($this->routes as $registeredRoute) {
			$result = $registeredRoute->resolve($path);
			if ($result !== false)
				return $result;
		}

		return false;
	}
}