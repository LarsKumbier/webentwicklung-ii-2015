<?php

namespace Router;

class Regexp extends AbstractRouter
{
	public function resolve($path) 
	{
		if (preg_match($this->path, $path))
			return $this->controller;
		return false;
	}
}
