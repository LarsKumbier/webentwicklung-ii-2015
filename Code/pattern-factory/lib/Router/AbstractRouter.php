<?php

namespace Router;

abstract class AbstractRouter implements IRoute
{
	protected $path;
	protected $controller;

	public function __construct($path, $controller) 
	{
		if (!is_string($path) || !is_string($controller))
			throw new \InvalidArgumentException('Parameters have to be string');
		$this->path = $path;
		$this->controller = $controller;
	}
}

