<?php

namespace Router;

class Fix extends AbstractRouter
{
	public function resolve($path) 
	{
		if (strtolower($path) == strtolower($this->path))
			return $this->controller;
		return false;
	}
}