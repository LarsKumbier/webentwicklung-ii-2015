<?php

namespace Router;

interface IRoute 
{
	public function __construct($path, $controller);
	public function resolve($path);
}

