<?php

namespace Kits\Db;

class SQLite implements IDb
{
	public function getIdentifier()
	{
		return 'SQLite';
	}
}