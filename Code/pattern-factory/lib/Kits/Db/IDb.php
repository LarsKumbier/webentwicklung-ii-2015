<?php

namespace Kits\Db;

interface IDb
{
	public function getIdentifier();
}