<?php

namespace Kits\Db;

class Factory
{
	public static function getInstance($db)
	{
		$classname = '\\Kits\\Db\\'.$db;
		return new $classname();
	}
}