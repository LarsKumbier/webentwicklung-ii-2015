<?php

// Wir nutzen: Traits, Array-Konstanten - daher Minimalversion 5.6
if (version_compare(phpversion(), '5.6.0', '<'))
	die('This Program requires a minimum PHP version of 5.6.0 due to it\'s use of array constants.');

// Startet den Autoload-Prozess, den wir in der composer.json-Datei
// definiert haben.
define ('APP_DIR', realpath(__DIR__.'/../'));

// Lade Autoloading
require_once(APP_DIR . '/vendor/autoload.php');

// Lade die Konfiguration
require_once(APP_DIR . '/config.php');




