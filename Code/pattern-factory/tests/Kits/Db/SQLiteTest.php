<?php

namespace Kits\Db;

class SQLiteTest extends \PHPUnit_Framework_TestCase
{
	public function test_GetIdentifierReturnsSQLite()
	{
		$db = new SQLite();
		$expected = 'SQLite';
		$output = $db->getIdentifier();

		$this->assertEquals($expected, $output);
	}
}