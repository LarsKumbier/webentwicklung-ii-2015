<?php

namespace Kits\Db;

class MysqlTest extends \PHPUnit_Framework_TestCase
{
	public function test_GetIdentifierReturnsMysql()
	{
		$db = new Mysql();
		$expected = 'Mysql';
		$output = $db->getIdentifier();

		$this->assertEquals($expected, $output);
	}
}