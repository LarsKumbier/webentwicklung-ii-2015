<?php

namespace Kits\Db;

class FactoryTest extends \PHPUnit_Framework_TestCase
{
	public function test_FactoryReturnsInstanceOfMysql()
	{
		$input = 'Mysql';
		$output = Factory::getInstance($input);
		$expected = '\Kits\Db\Mysql';

		$this->assertInstanceOf($expected, $output);
	}

	public function test_FactoryReturnsInstanceOfPostgresql()
	{
		$input = 'Postgresql';
		$output = Factory::getInstance($input);
		$expected = '\Kits\Db\Postgresql';

		$this->assertInstanceOf($expected, $output);
	}
}