<?php

namespace Traits;

define('TEST_PROPERTY_STANDARD', 23);

class MagicSetterGetterTest extends \PHPUnit_Framework_TestCase
{
	protected $test = null;

	public function setUp()
	{
		$this->test = new TestClass();
	}

	public function tearDown()
	{
		unset($this->test);
	}


	public function test_CanSetPropertyDirectly()
	{
		$this->test->property = 42;
		$this->assertEquals(42, $this->test->property);
	}

	public function test_CanSetPropertyViaSetter()
	{
		$this->test->setPropertyWithSetterAndGetter('moo');
		$this->assertTrue($this->test->setterCalled);
	}

	public function test_CanGetPropertyDirectly()
	{
		$this->assertEquals(TEST_PROPERTY_STANDARD, $this->test->property);
	}

	public function test_CanGetPropertyViaGetter()
	{
		$this->test->getPropertyWithSetterAndGetter();
		$this->assertTrue($this->test->getterCalled);
	}

	public function test_ThrowsNoticeWhenSettingUndefinedProperty()
	{
		$this->setExpectedException('PHPUnit_Framework_Error_Notice');
		$this->test->nonexistant = 23;
	}

	public function test_ThrowsNoticeWhenGettingUndefinedVarialbe()
	{
		$this->setExpectedException('PHPUnit_Framework_Error_Notice');
		$var = $this->test->nonexistant;
	}
	
}


class TestClass
{
	use MagicSetterGetter;

	protected $_property = TEST_PROPERTY_STANDARD;
	protected $_propertyWithSetterAndGetter = TEST_PROPERTY_STANDARD;

	public $setterCalled = false;
	public $getterCalled = false;

	public function getPropertyWithSetterAndGetter()
	{
		$this->getterCalled = true;
	}

	public function setPropertyWithSetterAndGetter($value)
	{
		$this->setterCalled = true;
	}
}