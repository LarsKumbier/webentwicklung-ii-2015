<?php

namespace ILearnTraits;

class ILearnTraitsTest extends \PHPUnit_Framework_TestCase
{
	public function test_ThisIsHowTraitsWork()
	{
		$neo = new Neo();
		$this->assertEquals('smash', $neo->kick('Morpheus'));
		$this->assertEquals('fomp', $neo->punch('Agent Smith'));
	}
}

class Neo
{
	use KungfuKick;
	use KungfuPunch;
}

trait KungfuKick
{
	public function kick($target) {
		return 'smash';
	}
}

trait KungfuPunch
{
	public function punch($target) {
		return 'fomp';
	}
}