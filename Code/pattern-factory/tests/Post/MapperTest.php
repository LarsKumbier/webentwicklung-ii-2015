<?php

namespace Post;

class MapperTest extends \PHPUnit_Framework_TestCase
{
	protected $pdo;
	protected $mapper;
	protected $defaultData = array (
			1 => array(':id' => 1, ':title' => 'First!', ':content' => 'Lorem Ipsum Baby', ':posttime' => '2015-01-01'),
			2 => array(':id' => 2, ':title' => 'Second!', ':content' => 'Lorem Ipsum Baby II', ':posttime' => '2015-02-02'),
			3 => array(':id' => 3, ':title' => 'Third!', ':content' => 'Lorem Ipsum Baby III', ':posttime' => '2015-03-03'),
			);

	public function setUp()
	{
		$pdo = $this->getPDO();
		$pdo->exec('CREATE TABLE posts (id INTEGER PRIMARY KEY, title TEXT, content TEXT, posttime DATETIME)');
		$stmt = $pdo->prepare('INSERT INTO posts (id,title,content,posttime) VALUES (:id, :title, :content, :posttime)');

		foreach ($this->defaultData as $row)
			$stmt->execute($row);

		$this->pdo = $pdo;
		$this->mapper = new Mapper($pdo);
	}

	public function tearDown()
	{
		$this->pdo = null;
		$this->mapper = null;
	}

	protected function getPDO()
	{
		$pdo = new \PDO( 'sqlite::memory:');
		$pdo->setAttribute(\PDO::ATTR_ERRMODE, \PDO::ERRMODE_EXCEPTION);
		$pdo->setAttribute(\PDO::ATTR_DEFAULT_FETCH_MODE, \PDO::FETCH_ASSOC);
		return $pdo;
	}


	public function test_CanAddPdoClassToConstructor()
	{
		$pdo = $this->getPDO();

		$mapper = new Mapper($pdo);

		$this->assertInstanceOf('\Post\Mapper', $mapper);
	}

	public function test_Fetch()
	{
		$model = $this->mapper->fetch(2);
		$this->assertEquals($this->defaultData[2][':title'], $model->title);
	}

	public function test_FetchAll()
	{
		$models = $this->mapper->fetchAll();
		$this->assertInternalType('array', $models);
		$this->assertCount(count($this->defaultData), $models);
	}
}