<?php

namespace Post;

class ModelTest extends \PHPUnit_Framework_TestCase
{
	public function test_CanCreateValidModel()
	{
		$model = new Model('Title', 'Content');
		$model = new Model('Title', 'Content', '2015-01-01');
		$model = new Model('Title', 'Content', '2015-01-01', 15);
		$model = new Model('Title', 'Content', null, 15);
	}

	public function test_CannotCreateModelWithMissingTitle()
	{
		$this->setExpectedException('\InvalidArgumentException');
		$model = new Model('', 'Content');
	}

	public function test_CannotCreateModelWithMissingContent()
	{
		$this->setExpectedException('\InvalidArgumentException');
		$model = new Model('Title', '');
	}

	public function test_DatePropertyIsADateClass()
	{
		$model = new Model('Title', 'Content', '2015-01-01');
		$expected = '2015-01-01';
		$actual = $model->posttime;
		$this->assertInstanceOf('\DateTime', $actual);
		$this->assertStringStartsWith($expected, $actual->format(\DateTime::ISO8601));
	}
}