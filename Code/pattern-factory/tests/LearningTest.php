<?php

class LearningTest extends \PHPUnit_Framework_TestCase
{
	public function test_DefaultBehaviourProperties()
	{
		$class = new TestClass();
		$this->assertEquals(15, $class->getId());
	}

	/**
	 * @see http://www.if-not-true-then-false.com/2012/php-pdo-sqlite3-example/
	 * Creds to Marvin
	 */
	public function test_PdoSqliteInMemory()
	{
		$pdo = new \PDO( 'sqlite::memory:');
		$pdo->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);

		// @see: http://php.net/manual/de/pdostatement.fetch.php#fetch_style
		$pdo->setAttribute(PDO::ATTR_DEFAULT_FETCH_MODE, PDO::FETCH_ASSOC);
		$this->assertInstanceOf('\PDO', $pdo);

		// Wir erstellen die Tabelle
		$return = $pdo->exec('CREATE TABLE posts (id INT PRIMARY KEY, title TEXT)');
		$this->assertEquals(0, $return);
		
		// Wir befüllen sie mit Testdaten
		$insertStatement = $pdo->prepare('INSERT INTO posts VALUES (:id, :title)');
		$this->assertInstanceOf('\PDOStatement', $insertStatement);
		$insertStatement->bindValue(':id',    1, SQLITE3_INTEGER);
		$insertStatement->bindValue(':title', 'Post Number One');
		$result = $insertStatement->execute();
		$this->assertEquals(1, $result);	// 1 ist die Anzahl der betroffenen Zeilen (wurde genau ein neuer Eintrag angelegt?)

		// Zweite Runde: Die Query bleibt gleich, aber wir schieben andere Daten rein.
		// Dabei nutzen wir als Binding direkt die execute(...)-Funktion
		$result = $insertStatement->execute(array(':id' => 2, ':title' => 'Post Number Two'));
		$this->assertEquals(1, $result);

		// Abrufen von Ergebnissen
		$posts = $pdo->query('SELECT id,title FROM posts ORDER BY id ASC');
		$expected = array (
			array ('id' => 1, 'title' => 'Post Number One'),
			array ('id' => 2, 'title' => 'Post Number Two')
			);
		foreach ($posts as $id => $row) {
			//var_dump($row);
			$this->assertEquals($expected[$id]['id'], $row['id']);
			$this->assertEquals($expected[$id]['title'], $row['title']);
		}

		/* Wir lernen:
		 *  - $pdo->prepare(...sqltemplate...) gibt uns statements 
		 *  - $statement->execute(array(...)) befüllt die templates und gibt die Anzahl der Ergebnisse zurück
		 *  - $pdo->query(...sql...) führt eine query aus und gibt die Ergebnisse zurück - über die können wir mit foreach iterieren
		 */ 
	}

	public function test_WhatIsThePDOInterface()
	{
		$test = new TestClass();
		$this->assertContains('iTest', class_implements($test));

		//$pdo = new \PDO('sqlite::memory:');
		//var_dump(class_implements($pdo));
	}

	public function test_Inheritance()
	{
		$test = new TestClass();
		$test->vererbung(new MyPDO('sqlite::memory:'));
	}

	public function test_DateTimeDoesWhatIThinkItDoes()
	{
		$date = new \DateTime('31.01.2015');
		$actual = $date->format(\DateTime::ISO8601);
		$expected = '2015-01-31';
		$this->assertStringStartsWith($expected, $actual);
	}

	public function test_WhatDoesDateTimeDoWhenAnInvalidDateIsGiven()
	{
		$this->setExpectedException('\Exception');
		new \DateTime('00.01.015');
	}
}

interface iTest {}

class TestClass implements iTest
{
	public $id = 15;

	function getId() {
		return $this->id;
	}

	public function vererbung(\PDO $pdo) {}
}

class MyPDO extends \PDO {}