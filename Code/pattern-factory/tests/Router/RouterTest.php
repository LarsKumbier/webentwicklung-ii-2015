<?php

namespace Router;

class RouterTest extends \PHPUnit_Framework_TestCase
{
	/**
	 * @group unit
	 */
	public function test_RouterFix()
	{
		$routefix = new Fix('/moo', 'XController');
		$actual = $routefix->resolve('/moo');
		$this->assertSame('XController', $actual);

		$actual = $routefix->resolve('/nonexistant');
		$this->assertFalse($actual);
	}

	/**
	 * @group unit
	 */
	public function test_RouterFixIgnoresCase()
	{
		$routefix = new Fix('/Moo', 'XController');
		$actual = $routefix->resolve('/moo');
		$this->assertSame('XController', $actual);
	}


	/**
	 * @group unit
	 */
	public function test_RouterRegexp()
	{
		$routeRegexp = new RegExp('/^\/user[0-9]$/', 'YController');
		$actual = $routeRegexp->resolve('/user1');
		$this->assertSame('YController', $actual);

		$actual = $routeRegexp->resolve('/userM');
		$this->assertFalse($actual);
	}


	/**
	 * @group integration
	 */
	public function test_Integration()
	{
		// =========== Arrange ===========
		$route = new Router();
		$route->addRoute(new Fix('/40Jahre', 'StaticController'))
		      ->addRoute(new Fix('/SpecialAktion', 'StaticController'))
		      ->addRoute(new RegExp('/^\/user\/[0-9]{0,6}\/?$/', 'UserController'));
		
		// Kombination: (1) Act -> (2) Assert
		$url = '/40jahre';
		$result = $route->getResponsibleController($url);
		$this->assertEquals('StaticController', $result);

		$url = '/SpecialAktion';
		$result = $route->getResponsibleController($url);
		$this->assertEquals('StaticController', $result);

		$url = '/user/198763';
		$result = $route->getResponsibleController($url);
		$this->assertEquals('UserController', $result);

		$url = '/user/';
		$result = $route->getResponsibleController($url);
		$this->assertEquals('UserController', $result);

		$url = '/user/1234/'; 
		$result = $route->getResponsibleController($url);
		$this->assertEquals('UserController', $result);

		$url = '/user/1234/asdf'; 
		$result = $route->getResponsibleController($url);
		$this->assertFalse($result);

		// Default Verhalten testen: Wenn nichts gefunden, dann False.
		// Alternative: 404Controller? ErrorController404?
		$url = '/non-existant-path/go/to/hell';
		$result = $route->getResponsibleController($url);
		$this->assertFalse($result);
	}

	/**
	 * @dataProvider dp_Regexp
	 * @group learning
	 */
	public function test_LearningRegexp($regexp, $input, $expected)
	{
		$actual = preg_match($regexp, $input);
		$this->assertSame($expected, $actual);
	}

	/**
	 * @group learning
	 */
	public function dp_Regexp()
	{
		return array (
			array("/foo/", "foo", 1),				// foo beinhaltet foo
			array("/foo/", "barfoobar", 1),			// barfoobar beinhaltet foo
			array("/^foo/", "barfoobar", 0),		// barfoobar beginnt nicht mit foo
			array("/^foo/", "foobar", 1),			// foobar beginnt mit foo
			array("/^foo/", "Foobar", 0),			// regexp sind standard case-sensitive...
			array("/^foo/i", "Foobar", 1),			// ... können aber auch insensitive gemacht werden
			array("/foo$/", "Foobar", 0),			// Foobar endet mit foo
			array("/foo$/", "Foobarfoo", 1),		// 
			array("/^\/user\//", '/user/1', 1),		// 
			array("/^\/user\//", '/profil/1', 0),	// 
			array("/^\/user\/[0-6]/", '/user/1', 1),// 
			array("/^\/user\/[0-6]/", '/user/7', 0),// 
			array("/^\/user\/[0-6]/", '/user/9999', 0),// 
			array("/^\/user\/[0-9]/", '/user/9999', 1),// 
			array("/^\/user\/[0-9]$/", '/user/09', 0),// 
			array("/^\/user\/[0-9]{0,5}$/", '/user/09234', 1),// 
			array("/^\/user\/[0-9]{0,5}$/", '/user/3', 1),// 
			array("/^\/user\/[0-9]{0,5}$/", '/user/', 1),// 
			array("/^\/user\/[0-9]{0,5}$/", '/user/3/', 0),// 
			array("/^\/user\/[0-9]{0,5}\/$/", '/user/3/', 1),// 
			array("/^\/user\/[0-9]{0,5}\/$/", '/user/3', 0),// 
			array("/^\/user\/[0-9]{0,5}\/?$/", '/user/3', 1),// 
			array("/^\/user\/[0-9]{0,5}\/?$/", '/user/3/', 1),// 
			array("/^\/user\/[0-9]{0,5}\/?$/", '/user/3//', 0),// 
			);
	}
}
