<?php

require_once('../lib/Bootstrap.php');

// setup router
$router = new Router\Router();
$router->addRoute(new Router\RegExp('/^\/post\/[0-9]{0,6}\/?$/', '\\Post\\Controller'));

// Routing
$path = parse_url($_SERVER['REQUEST_URI'], PHP_URL_PATH);
$controller = $router->getResponsibleController($path);

if ($controller === false) {
	require_once('404.php');
	exit();
}

// @todo: Datenbankobjekt erzeugen und übergeben
$controller = new $controller($_SERVER['REQUEST_URI']);
//$contoller->run();