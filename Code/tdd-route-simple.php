<?php

class RouteTest extends \PHPUnit_Framework_TestCase
{
	public function test_Integration()
	{
		// =========== Arrange ===========
		$route = new Router();

		// Fixe Route: /40jahre => StaticController
		$this->assertFalse($route->routeExists('/40jahre'));
		$route->addRouteFix('/40jahre', 'StaticController');
		$this->assertTrue($route->routeExists('/40jahre'));

		// Fixe Route: /SpecialAktion => StaticController
		$route->addRouteFix('/SpecialAktion', 'StaticController');

		// Regexp-Route für UserController
		$route->addRouteRegexp('/^\/user\/[0-9]{0,6}\/?$/', 'UserController');

		
		// Kombination: (1) Act -> (2) Assert
		$url = '/40jahre';
		$result = $route->getResponsibleController($url);
		$this->assertEquals('StaticController', $result);

		$url = '/SpecialAktion';
		$result = $route->getResponsibleController($url);
		$this->assertEquals('StaticController', $result);

		$url = '/user/198763';
		$result = $route->getResponsibleController($url);
		$this->assertEquals('UserController', $result);

		$url = '/user/';
		$result = $route->getResponsibleController($url);
		$this->assertEquals('UserController', $result);

		$url = '/user/1234/'; 
		$result = $route->getResponsibleController($url);
		$this->assertEquals('UserController', $result);

		$url = '/user/1234/asdf'; 
		$result = $route->getResponsibleController($url);
		$this->assertFalse($result);

		// Default Verhalten testen: Wenn nichts gefunden, dann False.
		// Alternative: 404Controller? ErrorController404?
		$url = '/non-existant-path/go/to/hell';
		$result = $route->getResponsibleController($url);
		$this->assertFalse($result);
	}

	/**
	 * @dataProvider dp_Regexp
	 * @group learning
	 */
	public function test_LearningRegexp($regexp, $input, $expected)
	{
		$actual = preg_match($regexp, $input);
		$this->assertSame($expected, $actual);
	}

	public function dp_Regexp()
	{
		return array (
			array("/foo/", "foo", 1),				// foo beinhaltet foo
			array("/foo/", "barfoobar", 1),			// barfoobar beinhaltet foo
			array("/^foo/", "barfoobar", 0),		// barfoobar beginnt nicht mit foo
			array("/^foo/", "foobar", 1),			// foobar beginnt mit foo
			array("/^foo/", "Foobar", 0),			// regexp sind standard case-sensitive...
			array("/^foo/i", "Foobar", 1),			// ... können aber auch insensitive gemacht werden
			array("/foo$/", "Foobar", 0),			// Foobar endet mit foo
			array("/foo$/", "Foobarfoo", 1),		// 
			array("/^\/user\//", '/user/1', 1),		// 
			array("/^\/user\//", '/profil/1', 0),	// 
			array("/^\/user\/[0-6]/", '/user/1', 1),// 
			array("/^\/user\/[0-6]/", '/user/7', 0),// 
			array("/^\/user\/[0-6]/", '/user/9999', 0),// 
			array("/^\/user\/[0-9]/", '/user/9999', 1),// 
			array("/^\/user\/[0-9]$/", '/user/09', 0),// 
			array("/^\/user\/[0-9]{0,5}$/", '/user/09234', 1),// 
			array("/^\/user\/[0-9]{0,5}$/", '/user/3', 1),// 
			array("/^\/user\/[0-9]{0,5}$/", '/user/', 1),// 
			array("/^\/user\/[0-9]{0,5}$/", '/user/3/', 0),// 
			array("/^\/user\/[0-9]{0,5}\/$/", '/user/3/', 1),// 
			array("/^\/user\/[0-9]{0,5}\/$/", '/user/3', 0),// 
			array("/^\/user\/[0-9]{0,5}\/?$/", '/user/3', 1),// 
			array("/^\/user\/[0-9]{0,5}\/?$/", '/user/3/', 1),// 
			array("/^\/user\/[0-9]{0,5}\/?$/", '/user/3//', 0),// 
			);
	}

	public function test_IntegrationUltimateSolution()
	{
		$this->markTestSkipped();
		// ============== Arrange ist leicht anders ===============
		$route = new Router();
		$route->addRoute(new Router\Fix('/40Jahre', 'StaticController'))
		      ->addRoute(new Router\Fix('/SpecialAktion', 'StaticController'))
		      ->addRoute(new Router\RegExp('/^\/user\/[0-9]{0,6}\/?$/', 'UserController'));

		// $route geht sein array aus Router\*-Objekten durch und ruft die Methode (von
		// den Objekten) "resolve($path)" auf. `resolve($path)` gibt entweder einen String
		// oder false zurück - bei false, wird die nächste Route getestet.

		// Selbe tests wie oben.
	}
}

class Router {
	protected $routes = array();

	public function addRouteFix($path, $controller, $update=false) 
	{
		if (!$update && $this->routeExists($path))
			throw new Exception("Path $path already has a controller registered: ".$this->routes[$path]);

		$route = array(
			'type' => 'fix',
			'path' => $path,
			'controller' => $controller);
		$this->routes[] = $route;

		return $this;
	}

	public function routeExists($route) 
	{
		foreach ($this->routes as $registeredRoute)
			if ($registeredRoute['path'] == $route)
				return true;
		return false;
	}

	public function addRouteRegexp($path, $controller, $update=false) 
	{
		if (!$update && $this->routeExists($path))
			throw new Exception("Path $path already has a controller registered: ".$this->routes[$path]);

		$route = array(
			'type' => 'regexp',
			'path' => $path,
			'controller' => $controller);

		$this->routes[] = $route;

		return $this;
	}

	public function getResponsibleController($path) 
	{
		foreach ($this->routes as $registeredRoute) {
			if ($registeredRoute['type'] == 'fix' && $registeredRoute['path'] == $path)
				return $registeredRoute['controller'];
			if ($registeredRoute['type'] == 'regexp' && preg_match($registeredRoute['path'], $path))
				return $registeredRoute['controller'];
		}
		return false;
	}
}