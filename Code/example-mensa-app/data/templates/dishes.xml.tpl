<?xml version="1.0" encoding="UTF-8"?>
<dishes xmlns="http://www.datenlotsen.de" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" canteenid="0" additivesGroup="0">
    {$index = 0}
    {foreach $dishes as $dish}
    <dish id="{$index++}">
        <name>{$dish.name}</name>
        <category>{$dish.category}</category>
        <day>{$dish.date}</day>
        <priceGuest>{$dish.priceGuest}</priceGuest>
        <priceOfficial>{if strlen($dish.priceOfficial) < 4}{$dish.priceStudent}{else}{$dish.priceOfficial}{/if}</priceOfficial>
        <priceStudent>{$dish.priceStudent}</priceStudent>
    </dish>
    {/foreach}
</dishes>