TODOs
=====
* change to git-flow repository layout
* Log invalid rows during conversion
* Delete old files from data/uploads and data/generatedXmls via Script (Cronjob)
* Minimize XML files
* Ignore rows with a date before this week
* Web-Interface to Upload files
* Web-Interface to link files from the upload directory
