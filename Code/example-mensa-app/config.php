<?php

/**
 * NOTICE: Please do NOT put a trailing slash to directories!
 *    wrong: define('FOO', '/data/');
 *    right: define('FOO', '/data');
 */

// where are the uploaded Excel sheets?
define('APP_UPLOAD_DIR', __DIR__.'/data/uploads');

// what is a pattern to filter correct dish filenames?
define('APP_DISHES_PREG', '/^dishes\-\d{6}(\d{2})?(\d{2})?(\d{2})?.[a-z]{3,4}$/i');

// where should I put the generated XML files?
define('APP_XML_DIR', __DIR__.'/data/generatedXmls');

// where is the folder that is the root of the webserver public directory?
define('APP_PUBLIC_DIR', __DIR__.'/public');

// where is the data directory?
define('APP_DATA_DIR', __DIR__.'/data');

// what is the relative path from the public directory to the XML directory?
define('APP_LINK_DIR', '../data/generatedXmls');
