Informationen zu der Mensa Anbindung an die CampusNet Mobile iOS App
v1.3

Allgemeines:
	- Die CampusNet Mobile App ruft die Daten vorläufig selbst von einem der Universität bereitgestellten Server ab. In einer späteren Version werden die Daten auf einem von den Datenlotsen bereitgestellten Server gecached und von dort von der App abgerufen.
	- Der Zugriff auf die Dateien darf nicht durch HTTP Auth Basic/Digest oder ähnliches beschränkt sein.

Canteens-XML:
	- Für jede Mensa kann jeweils ein Bild für das iPhone und das iPad angegeben werden. Folgende Bilder größen sind derzeit vorgesehen: iPhone 600x400 und iPad 1360x400. Die Grafiken werden automatisch skaliert um sie auf den verschiedenen Geräten und Ausrichtungen darzustellen.

Dishes-XML:
	- Die Kategorien für die Gerichte können von der Universtität frei gewählt werden.
	- Die Preise werden in der App so dargestellt, wie in der XML-Datei angegeben. Preise sollten daher zwei Nachkommastellen und das € Zeichen dahinter enthalten. Bsp.: <priceGuest>3,30€</priceGuest>
	- Es muss für jede Mensa eine eigene dishes_<canteenid>.xml Datei geben, mit der entsprechenden canteenID im root-Tag.

Additives-XML:
	- Die Angabe von Zusatzstoffen (additives) ist optional.
	- Es können mehrere Gruppen (additivesGroups) definitiert werden für den Fall das sich Zusatzstoffe und dessen Nummerierungen zwischen verschiedenen Mensen unterscheiden.
