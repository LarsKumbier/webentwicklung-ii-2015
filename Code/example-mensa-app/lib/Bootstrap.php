<?php
/**
 * Created by PhpStorm.
 * User: Lars Kumbier <lars@kumbier.it>
 * Date: 21.08.14
 * Time: 12:45
 */

define ('APP_DIR', __DIR__.'/../');
set_include_path(__DIR__ . PATH_SEPARATOR . get_include_path());
require_once(APP_DIR.'/vendor/autoload.php');
require_once(APP_DIR.'/config.php');