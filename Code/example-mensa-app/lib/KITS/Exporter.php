<?php
/**
 * Created by PhpStorm.
 * User: Lars
 * Date: 21.08.14
 * Time: 00:37
 */

namespace KITS;


class Exporter {
    private $outputFile = null;
    private $templateFile = 'dishes.xml.tpl';
    private $schemaFile = 'dishes.xsd';
    private $smarty = null;

    public function __construct($smarty = null, $xsdFile = null)
    {
        $this->smarty = $smarty === null ? new Exporter\Smarty : $smarty;
        if ($xsdFile !== null)
            $this->schemaFile = $xsdFile;
    }

    public function validateSchema ($xmlFile, $xsdFile)
    {
        if (!is_file($xmlFile) || !is_readable($xmlFile))
            throw new \InvalidArgumentException('Could not load XML file - not a file or not readable: '.$xmlFile);

        if (!is_file($xsdFile) || !is_readable($xsdFile))
            throw new \InvalidArgumentException('Could not load XSD file - not a file or not readable: '.$xsdFile);

        $xmlDom = new \DOMDocument();
        $xmlDom->load($xmlFile);
        return $xmlDom->schemaValidate($xsdFile);
    }

    public function generateXml($data, $outputFile = null)
    {
        if ($outputFile === null)
            $outputFile = tempnam(sys_get_temp_dir(), 'kits_exporter');

        $this->smarty->assign('dishes', $data);
        $xmlString = $this->smarty->fetch($this->templateFile);

        file_put_contents($outputFile, $xmlString);
        if (!$this->validateSchema($outputFile, $this->schemaFile))
            return false;

        if ($this->outputFile !== null)
            unlink($this->outputFile);
        $this->outputFile = $outputFile;

        return $outputFile;
    }

    public function __destruct()
    {
        if ($this->outputFile !== null)
            unlink($this->outputFile);
    }
} 