<?php
/**
 * Created by PhpStorm.
 * User: Lars
 * Date: 21.08.14
 * Time: 11:27
 */

namespace KITS\Exporter;


class Smarty extends \Smarty {
    public function __construct()
    {
        $dataDir = APP_DATA_DIR;
        parent::__construct();
        $this->setTemplateDir($dataDir.'/templates');
        $this->setCompileDir($dataDir.'/templates_c');
        $this->setConfigDir($dataDir.'/configs');
        $this->setConfigDir($dataDir.'/configs');
    }
} 