<?php
/**
 * Created by PhpStorm.
 * User: Lars Kumbier <lars@kumbier.it>
 * Date: 20.08.14
 * Time: 15:41
 */

namespace KITS;


class Importer {
    protected $filename = null;
    protected $mapping = array (
        null,
        null,
        'date',
        'category',
        'name',
        'priceGuest',
        'priceStudent',
        'priceOfficial'
    );

    public function setFilename($file)
    {
        if (!file_exists($file))
            throw new \InvalidArgumentException('File does not exist: '.$file);
        if (!is_readable($file))
            throw new \InvalidArgumentException('File is unreadable: '.$file);
        if (!is_file($file))
            throw new \InvalidArgumentException('Is not a file: '.$file);

        $this->filename = $file;

        return $this;
    }


    public function getFilename()
    {
        if ($this->filename === null)
            throw new Exception('No Filename set before usage');
        return $this->filename;
    }


    public function loadFile()
    {
        $filename = $this->getFilename();
        $excel = \PHPExcel_IOFactory::load($filename);
        if (($worksheet = $excel->getSheetByName('Speiseplan')) == false)
            throw new \InvalidArgumentException('Could not load Worksheet from Excel file: '.$filename);
        $data = $this->getDataFromWorksheet($worksheet);
        return $data;
    }

    protected function getDataFromWorksheet($worksheet)
    {
        $data = array();
        $numRows = $worksheet->getHighestRow();
        $numColumns = count($this->mapping);
        for ($row = 2; $row <= $numRows; $row++) {
            $entry = array();
            for ($col = 0; $col < $numColumns; $col++) {
                if ($this->mapping[$col] === null)
                    continue;

                $cell = $worksheet->getCellByColumnAndRow($col, $row);
                $value = $cell->getValue();

                $methodName = 'getValueFor'.ucfirst($this->mapping[$col]);
                if (\PHPExcel_Cell_DataType::dataTypeForValue($value) == 'f')
                    $value = null;

                if (method_exists($this, $methodName))
                    $value = $this->$methodName($value);

                $entry[$this->mapping[$col]] = $value;
            }
            if ($this->validateEntry($entry))
                $data[] = $entry;
        }

        return $data;
    }


    public function getValueForDate($exceldate)
    {
        $dateObj = \PHPExcel_Shared_Date::ExcelToPHPObject($exceldate);
        $date = date_format($dateObj, 'Y-m-d');
        return $date;
    }

    protected function getValueForPriceGuest($currency) {
        return $this->currencyToString($currency);
    }

    protected function getValueForPriceStudent($currency) {
        return $this->currencyToString($currency);
    }

    protected function getValueForPriceOfficial($currency) {
        return $this->currencyToString($currency);
    }

    public function currencyToString($currency)
    {
        if ($currency === null)
            return null;

        $result = sprintf('%01.2f', $currency).' €';
        return str_replace('.', ',', $result);
    }


    public function validateEntry($entry)
    {
        if (strptime($entry['date'], 'Y-m-d'))
            return false;
        if (strlen($entry['category']) < 2)
            return false;
        if (strlen($entry['name']) < 3)
            return false;
        if (!preg_match('/^\d+\,\d\d €$/', $entry['priceGuest']))
            return false;
        if (!preg_match('/^\d+\,\d\d €$/', $entry['priceStudent']))
            return false;
        if (!preg_match('/^\d+\,\d\d €$/', $entry['priceOfficial']) && $entry['priceOfficial'] !== null)
            return false;

        return true;
    }
} 