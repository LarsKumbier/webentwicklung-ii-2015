<?php
/**
 * Created by PhpStorm.
 * User: Lars
 * Date: 21.08.14
 * Time: 12:52
 */

namespace KITS;



use SebastianBergmann\Exporter\Exception;

class XmlFileManager {
    private $sourceDir = null;
    private $targetDir = null;

    public function setSourceDir($dir)
    {
        if (preg_match('/^\//', $dir)) {
            if (!is_dir($dir))
                throw new \InvalidArgumentException('Source Directory is not a directory: '.$dir);
            if (!is_readable($dir))
                throw new \InvalidArgumentException('Source Directory exists but is not readable: '.$dir);
        }

        $this->sourceDir = $dir;
        return $this;
    }


    public function setTargetDir($dir)
    {
        if (!is_dir($dir))
            throw new \InvalidArgumentException('Target Directory is not a directory: '.$dir);
        if (!is_readable($dir))
            throw new \InvalidArgumentException('Target Directory exists but is not readable: '.$dir);
        if (!is_writable($dir))
            throw new \InvalidArgumentException('Target Directory is not writable: '.$dir);

        $this->targetDir = $dir;
        return $this;
    }


    public function getActiveXml($filename)
    {
        $lastWD = getcwd();
        chdir($this->targetDir);
        $link = $this->targetDir.'/'.$filename;
        if (!is_link($link)) {
            trigger_error('No active xml found for filename: '.$link, E_USER_NOTICE);
            return false;
        }

        $target = readlink($link);
        if (!is_file($target)) {
            trigger_error('The xml links target for '.$link.' does not point to a file: '.$target, E_USER_WARNING);
            return false;
        }

        chdir($lastWD);

        return basename($target);
    }


    public function setActiveXml($sourcefile, $targetfile)
    {
        $sourcePath = $this->sourceDir.'/'.$sourcefile;
        $targetPath = $this->targetDir.'/'.$targetfile;

        $lastWD = getcwd();
        chdir($this->targetDir);

        if (!is_file($sourcePath))
            throw new \InvalidArgumentException('Source File does not exist: '.$sourcePath);
        if (!is_readable($sourcePath))
            throw new \InvalidArgumentException('Source File is not readable: '.$sourcePath);

        if (@$this->getActiveXml($targetfile) !== false)
            if (!unlink($targetPath))
                throw new Exception('Could not unlink target file: '.$targetPath);

        if (!symlink($sourcePath, $targetPath))
            throw new Exception('Could not relink source '.$sourcePath.' to target '.$targetPath);

        chdir($lastWD);
        return $this;
    }
} 