<?php
/**
 * Created by PhpStorm.
 * User: Lars
 * Date: 21.08.14
 * Time: 14:05
 */

function getNewestFile($dir, $pregPattern)
{
    if (!is_dir($dir))
        throw new \InvalidArgumentException('Not a directory: '.$dir);

    $dirFiles = scandir($dir, 0);
    $filteredFiles = array();
    foreach ($dirFiles as $file) {
        if (!preg_match($pregPattern, $file))
            continue;
        if (!is_file($dir.'/'.$file))
            continue;
        $filteredFiles[] = $file;
    }
    return array_pop($filteredFiles);
}