SRH App Mensa
=============
Author: Lars Kumbier <lars@kumbier.it>


Requirements
============
- php 5.3+
- composer (http://getcomposer.org)
- all other requirements are installed via composer


Installation
============
- clone to a directory outside the webserver document root
- direct your webserver vhost to the `public/` directory
- make sure, that symlinks are followed
- run `composer update` to install all required files
- make sure, that the `data/`- and `public/`-directories are writable by the (cronjob) user
- edit `public/additives.xml` to fit your needs
- edit `public/canteens.xml` to fit your needs - make your the canteenid is 0!


Usage
=====
- You can find a template of the Excel Input Sheet in `docs/speiseplan-vorlage.xlsx` - you might also use XLS or ODS format (untested)
- Put your Excel-Files into the folder `data/uploads` with the format `dishes-JJJJMMDD[HHIISS].extension` - e.g. `dishes-20140814.xlsx` or `dishes-201412301230.xlsx`
- run `scripts/update.php` - this generates the new dishes-xml-file in `data/generatedXmls` and links this file to `public/dishes_0.xml`. The old xml files are still present in data/generatedXmls
- if you overwrote an xlsx-file and would like to force a generation, use `scripts/update.php --force`
- if you would like to set a specific file, use `scripts/update.php --source /tmp/newFile.xlsx`


Troubleshooting
===============
Invalid rows in the Excel sheet are silently ignored and do not show up in the final xml document.
So, if a row does not show up, check for errors in the cells - e.g. a formular instead of a value
or a missing category.


Directory Structure
===================
- `data/`     Uploads of Excel files need to be here
- `docs/`     Documentation of datenlotsen
- `lib/`      Application Libraries
- `public/`   Target for Web folder
- `scripts/`  Scripts folder for manual execution or via cronjob
- `tests/`    PHPunit tests
- `vendor/`   automatically filled via `composer update`