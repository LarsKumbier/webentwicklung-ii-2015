<?php
/**
 * Created by PhpStorm.
 * User: Lars Kumbier <lars@kumbier.it>
 * Date: 21.08.14
 * Time: 12:41
 */

require_once(__DIR__.'/../lib/Bootstrap.php');
require_once(__DIR__.'/../lib/KITS/Helpers.php');

$uploadsDir = APP_UPLOAD_DIR;
$xmlDir = APP_XML_DIR;
$publicDir = APP_PUBLIC_DIR;
$dataDir = APP_DATA_DIR;
$validSourcePreg = APP_DISHES_PREG;
$linkDir = APP_LINK_DIR;

// Handle CLI arguments
$options = getopt('fs:', array('force', 'source:'));
$optForce = array_key_exists('force', $options) || array_key_exists('f', $options);

$optSourceFile = null;
if (array_key_exists('s', $options))
    $optSourceFile = $options['s'];
else if (array_key_exists('source', $options))
    $optSourceFile = $options['source'];

try {
    if ($optSourceFile !== null) {
        if (!is_file($optSourceFile))
            throw new \InvalidArgumentException('Sourcefile is not a file: '.$optSourceFile);
        if (!is_readable($optSourceFile))
            throw new \InvalidArgumentException('Sourcefile is not readable: '.$optSourceFile);

        copy($optSourceFile, $uploadsDir.'/'.basename($optSourceFile));
        $sourceFile = basename($optSourceFile);
    } else {
        $sourceFile = getNewestFile($uploadsDir, $validSourcePreg);
    }

    $targetXmlFile = pathinfo($sourceFile, PATHINFO_FILENAME).'.xml';

    $xmlMgr = new \KITS\XmlFileManager();
    $xmlMgr->setSourceDir($linkDir)
           ->setTargetDir($publicDir);

    if (!$optForce && @$xmlMgr->getActiveXml('dishes_0.xml') == $targetXmlFile) {
        echo "dishes_0.xml is already linked to the target: $targetXmlFile. Nothing to do.\n";
        exit(0);
    } else if ($optForce && @$xmlMgr->getActiveXml('dishes_0.xml') == $targetXmlFile) {
        echo "dishes_0.xml is already linked to the target: $targetXmlFile. Continuing anyway due to --force.\n";
    }

    $importer = new \KITS\Importer();
    $data = $importer->setFilename($uploadsDir.'/'.$sourceFile)->loadFile();

    echo "generating $targetXmlFile from $sourceFile...\n";
    $exporter = new \KITS\Exporter(null, $dataDir.'/dishes.xsd');
    $xmlFile = $exporter->generateXml($data);
    copy($xmlFile, $xmlDir.'/'.$targetXmlFile);

    @$xmlMgr->setActiveXml($targetXmlFile, 'dishes_0.xml');

    echo "dishes_0.xml is now linked to $targetXmlFile.\n";
    exit(0);
} catch (\Exception $e) {
    file_put_contents('php://stderr', "An exception occured while running the script: ".$e->getMessage()."\n");
    file_put_contents('php://stderr', "Stack Trace:\n".$e->getTraceAsString()."\n");
    exit(1);
}