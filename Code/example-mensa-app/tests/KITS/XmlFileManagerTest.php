<?php
/**
 * Created by PhpStorm.
 * User: Lars
 * Date: 21.08.14
 * Time: 12:53
 */

namespace KITS;


class XmlFileManagerTest extends \PHPUnit_Framework_TestCase {
    private $xmlMgr = null;
    private $targetDir = null;
    private $sourceDir = null;

    public function setUp()
    {
        $this->xmlMgr = new XmlFileManager();
        $this->targetDir = $this->mkTmpDir('target');
        $this->sourceDir = $this->mkTmpDir('source');
        $this->xmlMgr->setSourceDir($this->sourceDir);
        $this->xmlMgr->setTargetDir($this->targetDir);
    }

    public function tearDown()
    {
        if (!$this->rmdir($this->sourceDir))
            $this->cleanupAndDie('Could not delete temporary directory: '.$this->sourceDir);
        if (!$this->rmdir($this->targetDir))
            $this->cleanupAndDie('Could not delete temporary directory: '.$this->targetDir);
        unset($this->xmlMgr);
    }

    public function test_GetActiveXml()
    {
        touch($this->sourceDir.'/dishes-20140814.xml');
        touch($this->sourceDir.'/dishes-20140816.xml');

        symlink($this->sourceDir.'/dishes-20140814.xml', $this->targetDir.'/dishes_0.xml');
        $this->assertEquals('dishes-20140814.xml', $this->xmlMgr->getActiveXml('dishes_0.xml'));
    }

    public function test_CannotGetActiveXmlIfNotSet()
    {
        $this->assertFalse(@$this->xmlMgr->getActiveXml('dishes_0.xml'));
    }


    /**
     * @depends test_GetActiveXml
     * @depends test_CannotGetActiveXmlIfNotSet
     */
    public function test_CanSetActiveXml()
    {
        touch($this->sourceDir.'/dishes-20140814.xml');
        touch($this->sourceDir.'/dishes-20140816.xml');
        $this->assertFalse(is_link($this->targetDir.'/dishes_0.xml'));
        $this->xmlMgr->setActiveXml('dishes-20140814.xml', 'dishes_0.xml');
        $this->assertTrue(is_link($this->targetDir.'/dishes_0.xml'));
        $this->assertEquals('dishes-20140814.xml', $this->xmlMgr->getActiveXml('dishes_0.xml'));
        $this->xmlMgr->setActiveXml('dishes-20140816.xml', 'dishes_0.xml');
        $this->assertEquals('dishes-20140816.xml', $this->xmlMgr->getActiveXml('dishes_0.xml'));
    }


    public function test_LinkageIsARelativePath()
    {
        touch($this->sourceDir.'/dishes-20140814.xml');
        touch($this->sourceDir.'/dishes-20140816.xml');

        $sourceDirParts = explode('/', $this->sourceDir);
        $sourceDirName = array_pop($sourceDirParts);
        $this->xmlMgr->setSourceDir('../'.$sourceDirName);

        $this->xmlMgr->setActiveXml('dishes-20140814.xml', 'dishes_0.xml');
        $link = readlink($this->targetDir.'/dishes_0.xml');
        $this->assertStringStartsWith('../'.$sourceDirName, $link);
    }


    protected function rmdir($dirname)
    {
        $dir_handle = false;
        if (is_dir($dirname)) {
            $dir_handle = opendir($dirname);
        }
        if (!$dir_handle) {
            return false;
        }
        while ($file = readdir($dir_handle)) {
            if ($file != '.' && $file != '..') {
                if (!is_dir($dirname . '/' . $file)) {
                    unlink($dirname . '/' . $file);
                } else {
                    $this->rmdir($dirname . '/' . $file);
                }
            }
        }
        closedir($dir_handle);
        rmdir($dirname);
        return true;
    }

    protected function mkTmpDir($prefix = null)
    {
        if ($prefix === null)
            $prefix = '';

        $tmpDir = sys_get_temp_dir().'/KITS_XmlFileManagerTest_'.$prefix.'_'.rand(100000000, 999999999);
        if (mkdir($tmpDir) === false)
            $this->cleanupAndDie('Could not create temporary directory: ' . $tmpDir, $tmpDir);
        return $tmpDir;
    }

    protected function cleanupAndDie($msg)
    {
        @$this->rmdir($this->sourceDir);
        @$this->rmdir($this->targetDir);
        die($msg."\n");
    }
}
 