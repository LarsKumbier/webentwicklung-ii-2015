<?php
/**
 * Created by PhpStorm.
 * User: Lars
 * Date: 20.08.14
 * Time: 15:41
 */

namespace KITS;

const MIN_FILE = 'speiseplan-valid-min.xlsx';

class ImporterTest extends \PHPUnit_Framework_TestCase {
    private $minfile;
    private $importer;

    public function setUp()
    {
        $this->minfile = dirname(__FILE__).'/ImporterTest/'.MIN_FILE;
        $this->importer = new Importer();
    }

    public function test_CanLoadAValidFile() {
        $this->assertInstanceOf('\KITS\Importer', $this->importer->setFilename($this->minfile));
    }

    public function test_CannotLoadNonExistantFile() {
        $this->setExpectedException('InvalidArgumentException', 'does not exist');
        $this->importer->setFilename('nonexistant');
    }

    /**
     * @group Integration
     * @medium
     */
    public function test_CanLoadValidFile() {
        //$this->markTestSkipped();

        $actual = $this->importer
                       ->setFilename($this->minfile)
                       ->loadFile();

        $expected = array (
            array(
                'date' => '2014-08-18',
                'category' => 'menü 1',
                'name' => 'Currywurst vom grill mit haugemachter Curysuce und Pommes',
                'priceGuest' => '5,00 €',
                'priceStudent' => '2,00 €',
                'priceOfficial' => '2,00 €'
                ),
            array (
                'date' => '2014-08-18',
                'category' => 'Menü 2',
                'name' => 'Gebratenes Seelachsfilet auf Zucchini-Tomatengemüse serviert mit Kräuter-Risotto',
                'priceGuest' => '4,00 €',
                'priceStudent' => '1,50 €',
                'priceOfficial' => null
                )
            );

        $this->assertEquals($expected[0], $actual[0], 'First data line is correct');
        $this->assertEquals($expected[1], $actual[1], 'Second data line is correct');
        $this->assertEquals(19, count($actual));
    }


    /**
     * @dataProvider dataProvider_ExcelDateConverter
     * @group Unit
     */
    public function test_ExcelDateConverter($input, $expected) {
        $this->assertEquals($expected, $this->importer->getValueForDate($input));
    }

    public function dataProvider_ExcelDateConverter() {
        return array (
            array(41869.0, '2014-08-18'),
            array(41869.9999, '2014-08-18'),
            array(42429.0, '2016-02-29'),
            array(42430.25, '2016-03-01')
        );
    }


    /**
     * @dataProvider dataProvider_CurrencyConverter
     * @group Unit
     */
    public function test_ExcelCurrencyConverter($input, $expected) {
        $this->assertEquals($expected, $this->importer->currencyToString($input));
    }

    public function dataProvider_CurrencyConverter()
    {
        return array (
            array(5.0, '5,00 €'),
            array(3.23, '3,23 €'),
            array(153.23, '153,23 €'),
            array(153.235, '153,24 €'),
            array(null, null)
        );
    }


    /**
     * @dataProvider dataProvider_validateEntries
     * @group Unit
     */
    public function test_validateEntry($input, $assertion)
    {
        $assertMethod = 'assert'.ucfirst($assertion);
        $this->$assertMethod($this->importer->validateEntry($input));
    }

    public function dataProvider_validateEntries()
    {
        return array(
            array (
                array(
                    'date' => '2014-08-18',
                    'category' => 'menü 1',
                    'name' => 'Currywurst vom grill mit haugemachter Curysuce und Pommes',
                    'priceGuest' => '5,00 €',
                    'priceStudent' => '2,00 €',
                    'priceOfficial' => '2,00 €'
                ),
                'true'
            ),
            array (
                array(
                    'date' => '2014-08-18',
                    'category' => 'menü 1',
                    'name' => 'Currywurst vom grill mit haugemachter Curysuce und Pommes',
                    'priceGuest' => '5,00 €',
                    'priceStudent' => '2,00 €',
                    'priceOfficial' => null
                ),
                'true'
            ),
            array (
                array(
                    'date' => '2014-08-18',
                    'category' => '',
                    'name' => 'Currywurst vom grill mit haugemachter Curysuce und Pommes',
                    'priceGuest' => '5,00 €',
                    'priceStudent' => '2,00 €',
                    'priceOfficial' => '2,00 €'
                ),
                'false'
            ),
            array (
                array(
                    'date' => '2014-08-18',
                    'category' => 'menü 1',
                    'name' => '',
                    'priceGuest' => '5,00 €',
                    'priceStudent' => '2,00 €',
                    'priceOfficial' => '2,00 €'
                ),
                'false'
            ),
            array (
                array(
                    'date' => '2014-08-18',
                    'category' => 'menü 1',
                    'name' => 'Currywurst vom grill mit haugemachter Curysuce und Pommes',
                    'priceGuest' => '',
                    'priceStudent' => '2,00 €',
                    'priceOfficial' => '2,00 €'
                ),
                'false'
            ),
            array (
                array(
                    'date' => '2014-08-18',
                    'category' => 'menü 1',
                    'name' => 'Currywurst vom grill mit haugemachter Curysuce und Pommes',
                    'priceGuest' => '5,00 €',
                    'priceStudent' => null,
                    'priceOfficial' => '2,00 €'
                ),
                'false'
            )
        );
    }
}
