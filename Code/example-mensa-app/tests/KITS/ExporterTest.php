<?php
/**
 * Created by PhpStorm.
 * User: Lars
 * Date: 21.08.14
 * Time: 00:37
 */

namespace KITS;

class ExporterTest extends \PHPUnit_Framework_TestCase
{
    private $xsdSchema = null;
    private $validXmlComplete = null;
    private $invalidXml = null;

    private $validData = array (
        array(
            'date' => '2014-08-18',
            'category' => 'menü 1',
            'name' => 'Currywurst vom grill mit haugemachter Curysuce und Pommes',
            'priceGuest' => '5,00 €',
            'priceStudent' => '2,00 €',
            'priceOfficial' => '2,00 €'
        ),
        array(
            'date' => '2014-08-18',
            'category' => 'Klassiker',
            'name' => 'Currywurst vom grill mit haugemachter Curysuce und Pommes',
            'priceGuest' => '5,00 €',
            'priceStudent' => '2,00 €',
            'priceOfficial' => null
        ),
        array(
            'date' => '2014-08-18',
            'category' => 'menü 3 (vegetarisch)',
            'name' => 'Salat mit Röstkartoffeln',
            'priceGuest' => '5,23 €',
            'priceStudent' => '3,50 €',
            'priceOfficial' => null
        )
    );

    private $exporter = null;

    public function setUp()
    {
        $this->xsdSchema = dirname(__FILE__).'/../../data/dishes.xsd';
        $this->validXmlComplete = dirname(__FILE__).'/ExporterTest/dishes_valid_with_annotations.xml';
        $this->invalidXml = dirname(__FILE__).'/ExporterTest/dishes_invalid.xml';
        libxml_use_internal_errors(true); // suppresses local error messages

        $this->exporter = new Exporter(null, $this->xsdSchema);
    }

    /**
     * @group Unit
     */
    public function test_Prerequisite_CanValidateAgainstSchema()
    {
        $this->assertTrue($this->exporter->validateSchema($this->validXmlComplete, $this->xsdSchema));
    }

    /**
     * @group Unit
     */
    public function test_Prerequisite_CanInvalidateAgainstSchema()
    {
        $this->assertTrue($this->exporter->validateSchema($this->invalidXml, $this->xsdSchema));
    }

    /**
     * @dataProvider dataProvider_ValidInputData
     */
    public function test_GeneratesExpectedXmlFromInputData($data, $expectedXmlFile)
    {
        $xmlFile = $this->exporter->generateXml($data);

        $actual = $this->prepareXmlForComparison(file($xmlFile));
        $expected = $this->prepareXmlForComparison(file(dirname(__FILE__).'/ExporterTest/'.$expectedXmlFile));

        $this->assertEquals($expected, $actual);
    }


    public function prepareXmlForComparison($xmlArray)
    {
        $result = '';
        foreach ($xmlArray as $line) {
            $line = trim($line);
            if (strlen($line) <= 1)
                continue;
            $result .= $line;
        }
        return $result;
    }


    public function test_DoesDeleteAllTemporaryFilesOnDestruction()
    {
        $filename = tempnam(sys_get_temp_dir(), 'kits_exportertest_');
        $this->exporter->generateXml($this->validData, $filename);
        $this->assertFileExists($filename);
        unset($this->exporter);
        $this->assertFileNotExists($filename);
    }



    public function dataProvider_ValidInputData()
    {
        return array (
            array (
                $this->validData,
                'dishes_ValidInputData.xml'
            )
        );
    }
}
 