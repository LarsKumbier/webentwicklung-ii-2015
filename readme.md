# Webentwicklung II

Nachdem wir uns im ersten Seminar *Webentwicklung I* primär um das Thema Design und Frontend gekümmert haben, geht es nun in *Webentwicklung II* vor allem um das Backend. Im Seminar werden wir uns sehr viel mit [Clean Code][ccd] und der Frage, was guten Code ausmacht und wie man nachhaltig programmiert, beschäftigen.



## Referenzen

[ccd]: http://www.clean-code-developer.de/ "Clean Code Initiative"